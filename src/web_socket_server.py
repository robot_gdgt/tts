import json
import threading
from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket

clients = []
webSocketCallbacks = None

class WebSocketHandlers(WebSocket):
    def handleConnected(self):
#         print(self.address, 'connected')
        clients.append(self)
        if webSocketCallbacks is not None:
            if hasattr(webSocketCallbacks, 'connected'):
                getattr(webSocketCallbacks, 'connected')(self)

    def handleClose(self):
#         print(self.address, 'closed')
        clients.remove(self)
        if webSocketCallbacks is not None:
            if hasattr(webSocketCallbacks, 'closed'):
                getattr(webSocketCallbacks, 'closed')(self)

    def handleMessage(self):
#         print(f'recd: {self.data}')
        try:
            self.data = json.loads(self.data)
        except json.JSONDecodeError:
            self.sendMessage('{"type":"error","errmsg":"failed to decode JSON"}')
            return

        if 'id' not in self.data:
            self.sendMessage('{"type":"error","errmsg":"id missing"}')
            return
        if 'type' not in self.data:
            self.sendMessage('{"type":"error","errmsg":"type missing"}')
            return

        self.last_id = self.data['id']

        if webSocketCallbacks is not None:
            if hasattr(webSocketCallbacks, self.data['type']):
                getattr(webSocketCallbacks, self.data['type'])(self)

    def send(self, type, data = None):
        self.sendMessage(json.dumps({
            'id': 0,
            'type': type,
            'data': data
        }))

    def reply(self, data = None, error = None):
        self.sendMessage(json.dumps({
            'id': 0,
            'type': 'reply',
            'reqid': self.last_id,
            'data': data,
            'error': error
        }))

class WebSocketThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        server = SimpleWebSocketServer('', 8000, WebSocketHandlers)
        server.serveforever()

def broadcast(type, data = None, skip = None):
#     print(f'broadcast({data}) to {len(clients)} clients')
    if clients:
        for client in clients:
            if client != skip:
                client.send(type, data)

server = WebSocketThread()
threading.Thread(target=server.run, daemon=True).start()
