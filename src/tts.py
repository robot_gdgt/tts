"""
Text-to-speech
"""

from datetime import datetime
import json
import logging
import math
from os import path
from pygame import mixer
import random
import sqlite3
# from sqlite3 import Error
import subprocess
from time import sleep, perf_counter

from can_ids import *
import can_proxy_thread as can
from gdgt_platform import *
from status_led import StatusLED
from tts_groups import *
# from var_dump import var_dump as vd

import web_socket_server

hbLED = StatusLED(LED_PIN, True)

# FILE = path.splitext(path.basename(__file__))[0]
# logger.init(None, logging.INFO)

# App state object class


class AppState:
    def __init__(self):
        self.state = CAN_CMD_PGM_RUNNING
        self.defaultVol = 0.2
        self.drive = 0  # 0 = not driving, 1 = waiting to center, 2 = driving
        self.driveFactor = 1
        self.canLogFP = None

state = AppState()

##### TTS Functions #####

REPEAT_DELAY = 4

lastPlayed = {}

'''
playSoundFromGroup(con, idgroup, vol)
    getFromGroup(con, idgroup) -> row
    playWav(file, vol) -> channel
    or
    sayString(id, str, vol) -> playWav
        playWav(file, vol) -> channel
'''


def sqlConnection():
    try:
        con = sqlite3.connect('/home/pi/gdgt_tts/src/strings.db')
        con.row_factory = sqlite3.Row
        return con
    except sqlite3.Error as e:
        logging.error(f'sqlite3.connect error: {e}')
        raise Exception('Failed to open sqlite db')

def logConnection():
    try:
        con = sqlite3.connect('/home/pi/gdgt_tts/src/strings.db')
        con.row_factory = sqlite3.Row
        return con
    except sqlite3.Error as e:
        logging.error(f'sqlite3.connect error: {e}')
        raise Exception('Failed to open sqlite db')


def getFromGroup(con, idgroup):
    logging.info(f"Say from group {idgroup}")
    cur = con.cursor()
    cur.execute("select count(*) from strings where idgroup = ?", (idgroup,))
    row = cur.fetchone()
    if row[0] == 0:
        logging.debug("Not found")
        return None
    n = random.randrange(0, math.trunc(row[0] / 2 + 0.5))
    cur.execute(
        "select id, text, last_used from strings where idgroup = ? order by last_used limit 1 offset ?", (idgroup, n))
    row = cur.fetchone()
    if row:
        #       logging.debug(row['id'])
        now = datetime.now()
        cur.execute("update strings set last_used = ? where id = ?",
                    (now.timestamp(), row['id']))
        con.commit()
        return row
    else:
        return None


def getFromID(con, id):
    logging.info(f"Say ID {id}")
    cur = con.cursor()
    cur.execute("select id, text from strings where id = ?", (id,))
    row = cur.fetchone()
    return row


def sayString(id, str, vol):
    #   logging.debug(str)
    cmd = f"pico2wave -w /home/pi/phrases/{id}.wav \"{str}\""
    logging.debug(cmd)
    subprocess.run([cmd], shell=True)
    return playWav(f"/home/pi/phrases/{id}.wav", vol)


def playWav(file, vol):
    if mixer.music.get_busy():
        logging.debug("Is busy")
        mixer.music.fadeout(500)
    logging.info(f"Play {file}")
    mixer.music.load(file)
    mixer.music.set_volume(vol)
    logging.debug(f"Play with vol {vol}")
    return mixer.music.play()  # returns channel


def playSoundFromGroup(con, idgroup, vol):
    now = datetime.now().timestamp()
    if idgroup in lastPlayed and now < lastPlayed[idgroup] + REPEAT_DELAY:
        return None

    snd = getFromGroup(con, idgroup)
    if snd is not None:
        lastPlayed[idgroup] = now
        if snd['text'].endswith('.wav'):
            logging.debug(f"/home/pi/gdgt_tts/sounds/{snd['text']}")
            return playWav(f"/home/pi/gdgt_tts/sounds/{snd['text']}", vol)
        else:
            if path.exists(f"/home/pi/phrases/{snd['id']}.wav"):
                return playWav(f"/home/pi/phrases/{snd['id']}.wav", vol)
            else:
                return sayString(snd['id'], snd['text'], vol)


def playSoundFromID(con, id, vol):
    snd = getFromID(con, id)
    if snd is not None:
        if snd['text'].endswith('.wav'):
            logging.debug(f"/home/pi/gdgt_tts/sounds/{snd['text']}")
            return playWav(f"/home/pi/gdgt_tts/sounds/{snd['text']}", vol)
        else:
            if path.exists(f"/home/pi/phrases/{snd['id']}.wav"):
                return playWav(f"/home/pi/phrases/{snd['id']}.wav", vol)
            else:
                return sayString(snd['id'], snd['text'], vol)

##### WEB SOCKET #####

class WebSocketCallbacks:
    def CANtx(self, client):
        try:
            print(client.data)
            CANtx = client.data['pkt']
            can.send('CANtx', CANtx['id'], CANtx['rr'], CANtx['data'])
        except Exception as e:
            print(e)

    def log(self, client):
        try:
            # print(client.data)
            if client.data['logActive']:
                state.canLogFP = open(f"/home/pi/canlogs/{client.data['logName']}.txt", "a")
                client.reply('log open')
            else:
                if state.canLogFP:
                    state.canLogFP.close()
                    state.canLogFP = None
                    client.reply('log closed')
        except Exception as e:
            print(e)

##### SETUP #####

if path.exists('tts.ini'):
    with open('tts.ini') as infile:
        data = json.load(infile)
else:
    data = None

if data:
    state.defaultVol = data['defaultVol']

web_socket_server.webSocketCallbacks = WebSocketCallbacks()

can.del_filter('CANrx', 'all', 'all')
# can.add_filter('CANrx', CAN_CMD_SPEAK, 0x7FF)
# can.add_filter('CANrx', CAN_SPEAK_DIRECT, 0x700)
# can.add_filter('CANrx', CAN_CMD_HALT, 0x7FF)
can.add_filter('CANrx', 0x000, 0x000)  # All IDs

random.seed()
mixer.init(16000)

con = sqlConnection()

playSoundFromGroup(con, SPEAK_STARTUP, state.defaultVol)

adhocStrings = {}

hbLED.heartbeat()

##### LOOP #####

# spinner = 0
# spinners = ['-', '\\', '|', '/']
# print('READY\n-', end='')
print('READY')

startTime = perf_counter()

while True:

    # now = time()

    # spinner = (spinner + 1) % 4
    # print(f'\b{spinners[spinner]}', end='')

    CANrx = can.get('CANrx')
    if CANrx is not None:
        # print(f'\nCAN Rx: {CANrx}')
        # CAN Rx: {'id': 268, 'rr': False, 'dl': 8, 'data': [101, 39, 101, 50, 129, 18, 129, 124]}

        CANpkt = {
            'millis': round((perf_counter() - startTime) * 1000),
            'id': CANrx['id'],
            'rr': 1 if CANrx['rr'] else 0,
            'dl': CANrx['dl'],
            'data': CANrx['data'],
            'out': 1 if 'sender' in CANrx and CANrx['sender'] == 'CANtx' else 0
        }

        web_socket_server.broadcast('CANrx', CANpkt)

        if state.canLogFP:
            state.canLogFP.write(f"{CANpkt['millis']},{CANpkt['id']},{CANpkt['rr']},{CANpkt['dl']},[{','.join([str(i) for i in CANpkt['data']])}],{CANpkt['out']}\n")
            state.canLogFP.flush()

        if CANrx['id'] == CAN_CMD_SPEAK:
            if CANrx['dl'] == 1:
                playSoundFromGroup(con, CANrx['data'][0], state.defaultVol)
            elif CANrx['dl'] > 1:
                if CANrx['data'][0] == 0:  # set volume
                    state.defaultVol = CANrx['data'][1] / 512
                    # TODO add saving state.defaultVol to config file and read it back in later
                    data = {'defaultVol': state.defaultVol}
                    with open('tts.ini', 'w') as outfile:
                        json.dump(data, outfile)
                else:
                    if CANrx['data'][0] == 1:
                        vol = state.defaultVol
                    else:
                        vol = CANrx['data'][0] / 512
                    d = 0
                    for i in range(1, CANrx['dl']):
                        d = d * 256 + CANrx['data'][i]
                    if d < 1000:
                        playSoundFromGroup(con, d, vol)
                    else:
                        playSoundFromID(con, d, vol)

        elif CANrx['id'] & 0x700 == CAN_SPEAK_DIRECT:
            adhocId = CANrx['id'] & 0x0FF
            if adhocId not in adhocStrings:
                adhocStrings[adhocId] = ''
            isFinal = False
            for c in CANrx['data']:
                if c == 10 or c == 0:  # new line or null terminator
                    isFinal = True
                    break
                adhocStrings[adhocId] += chr(c)
            if isFinal:
                if len(adhocStrings[adhocId]) > 0:
                    sayString('adhoc', adhocStrings[adhocId], state.defaultVol)
                    adhocStrings[adhocId] = ''

        elif CANrx['id'] == CAN_CMD_QUIT:
            channel = playSoundFromGroup(con, SPEAK_SHUTDOWN, state.defaultVol)
            logging.info('quit')
            sleep(5)
            state.state = CAN_CMD_QUIT

        elif CANrx['id'] == CAN_CMD_HALT:
            sayString('halt', 'Shutting down. Good night.', state.defaultVol)
            logging.info('shutdown')
            sleep(5)
            state.state = CAN_CMD_HALT

    # Blink the LED
    hbLED.tickle()

    if state.state == CAN_CMD_QUIT or state.state == CAN_CMD_HALT:
        break

if state.state == CAN_CMD_HALT:
    subprocess.run("sudo halt", shell=True)

"""
CAN data:
DLC == 1 means play sound from group 1 - 255 at default volume
data[0] == 0 set default volume = data[1]
data[0] == 1 play sound = data[1...n]
data[0] > 1 play sound = data[1...n] at vol = data[0]

sound = 1...999 = play randomly from group
sound > 1000 = play specific sound
"""
