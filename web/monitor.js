"use strict";

const selectId = $('#sendForm > select[name="id"]')[0];
let prev = '';
Object.keys(CAN_IDS).sort().forEach(id => {
  if (prev != id.substring(0, 1)) {
    selectId.appendChild(new Option('', ''));
    prev = id.substring(0, 1);
  }
  selectId.appendChild(new Option(`0x${id} ${CAN_IDS[id].name}`, id));
});
const $macroPanel = $('#macroPanel');
const templateMacro = $('#templateMacro').text();
macros.forEach(function(macro) {
  macro.json = JSON.stringify(macro);
  let rendered = Mustache.render(templateMacro, macro);
  $macroPanel.append(rendered);
});

let filterMask = 0x000; // Show all
let filterId = 0x000; // Show all
let rowCount = 0;

const templateRaw = $('#templateRaw').text();
const $tbodyRaw = $('#tableRaw > div.tbody');
const templateDecoded = $('#templateDecoded').text();
const $tbodyDecoded = $('#tableDecoded > div.tbody');

_(`ws://${location.hostname}:8000`);

var ws = new WebSocket(`ws://${location.hostname}:8000`);
ws.onopen = function (evt) {
  _('WebSocket onopen');
  $('.connected').css('background-color', 'rgb(0, 192, 65)');
};
ws.onclose = function (evt) {
  _('WebSocket onclose');
  $('.connected').css('background-color', 'rgb(255, 84, 82)');
};
ws.onerror = function (error) {
  _('WebSocket onerror: ', error);
};
ws.onmessage = function (evt) {
  const evt_data = JSON.parse(evt.data);
  // _('WebSocket onmessage: ', evt_data);
  if (evt_data.type == 'CANrx') {
    var pkt = evt_data.data;
    // _(pkt)
    decoder.process(pkt);
    pkt.trClass = `CAT${pkt.idhex.substring(0, 1)}`;
    pkt.json = JSON.stringify(pkt)
    addRows(pkt);
  } else {
    _('WebSocket onmessage: ', evt_data);
  }
};

const decoder = new Decoder();

var autoScroll = true;
var filterActive = false;
var hideBatt = true;
var currentViewIsRaw = false;
var logActive = false;
var logName = '';

$('.fillData').on('click', () => {
  let m = 0;
  Object.keys(CAN_IDS).sort().forEach(id => {
    let pkt = {
      millis: m++,
      id: id,
      rr: 0,
      dl: 4,
      data: [1, 2, 3, 4]
    };
    _(pkt)
    decoder.process(pkt);
    pkt.trClass = `CAT${pkt.idhex.substring(0, 1)}`;
    pkt.json = JSON.stringify(pkt);
    addRows(pkt);
  });
});

$('.btnToggleView').on('click', function (e) {
  if ($(this).data('decode') == '1') { // switching to decoded view
    const scrollTop = $tbodyRaw[0].scrollTop;
    $('#tableRaw').hide();
    $('#tableDecoded').show();
    $tbodyDecoded[0].scrollTop = scrollTop;
    currentViewIsRaw = false;
  } else { // switching to raw view
    const scrollTop = $tbodyDecoded[0].scrollTop;
    $('#tableDecoded').hide();
    $('#tableRaw').show();
    $tbodyRaw[0].scrollTop = scrollTop;
    currentViewIsRaw = true;
  }
});

$('.btnClear').on('click', function (e) {
  $('#tableRaw div.tbody, #tableDecoded div.tbody').empty();
  rowCount = 0;
  $('#rowCount').text(rowCount);
});

$('.cbFilter').on('click', function (e) {
  filterActive = this.checked;
  $('.cbFilter').prop('checked', this.checked);
  if (filterActive) {
    $('#filterForm').show();
    $('.tbody').css('bottom', 65);
    $('.tr').each((i, tr) => {
      let id = $(tr).data('pkt').id;
      if ((id & filterMask) != filterId) $(tr).hide();
    });
  } else {
    $('#filterForm').hide();
    $('.tbody').css('bottom', 32);
    $('.tr').each((i, tr) => {
      $(tr).show();
    });
    if (hideBatt) {
      $('.tr.ID10C').each((i, tr) => {
        $(tr).hide();
      });
    }
    // filterMask = 0x000; // Show all
    // filterId = 0x000; // Show all
  }
});

$('.cbHideBatt').on('click', function (e) {
  hideBatt = this.checked;
  $('.cbHideBatt').prop('checked', this.checked);
  if (hideBatt) {
    $('.tr.ID10C').each((i, tr) => {
      $(tr).hide();
    });
  } else {
    $('.tr.ID10C').each((i, tr) => {
      $(tr).show();
    });
  }
});

$('.cbLog').on('click', function (e) {
  logActive = this.checked;
  $('.cbLog').prop('checked', this.checked);
  if (logActive) {
    let temp = prompt('Enter the log name:', logName);
    if (temp !== null && temp.trim() != '') {
      logName = temp;
      ws.send(JSON.stringify({
        id: 0,
        type: 'log',
        logActive: true,
        logName: logName
      }));
    } else {
      $('.cbLog').prop('checked', false);
    }
  } else {
    ws.send(JSON.stringify({
      id: 0,
      type: 'log',
      logActive: false
    }));
  }
});

$('#filterBtn').on('click', function (e) {
  filterMask = parseInt($('#filterForm input[name="filterMask"]')[0].value, 16);
  filterId = parseInt($('#filterForm input[name="filterId"]')[0].value, 16) & filterMask;
  $('.tr').each((i, tr) => {
    let id = $(tr).data('pkt').id;
    if ((id & filterMask) == filterId) $(tr).show();
    else $(tr).hide();
  });
});

$('.cbAutoScroll').on('click', function (e) {
  autoScroll = this.checked;
  $('.cbAutoScroll').prop('checked', this.checked);
});

$('.tbody').on('click', '.resend', function (e) {
  const pkt = $(this).closest('.tr').data('pkt');
  _(pkt);
  selectId.value = pkt.idhex;
  $('#sendForm input[name="rr"]')[0].checked = pkt.rr;
  $('#sendForm input[name="data"]')[0].value = pkt.dataOut;
});

$('#sendBtn, #b1Btn, #b2Btn').on('click', function (e) {
  if (selectId.value == '') {
    alert('Select a packet ID first.');
    return;
  }
  let data = $('#sendForm input[name="data"]')[0].value.trim();
  if (data === '') {
    data = [];
  } else {
    data = data.split(' ');
    data.forEach((v, i) => {
      if (v.indexOf('0x') == 0) {
        data[i] = parseInt(v, 16);
      } else {
        data[i] = parseInt(v);
      }
    });
  }

  const id = parseInt(selectId.value, 16);
  const rr = $('#sendForm input[name="rr"]')[0].checked ? 1 : 0;
  ws.send(JSON.stringify({
    id: 0,
    type: 'CANtx',
    pkt: {
      id: id,
      rr: rr,
      data: data
    }
  }));
});

$('#sendForm input[name="data"]').on('keydown', function (e) {
  // _(e);
  if(e.key == 'Enter') {
    $('#sendBtn').trigger('click');
  }
});

$('button.btnToggleView[data-decode="1"]').trigger('click');

function addRows(pkt) {
  // let id = parseInt(pkt.id, 16);
  let id = pkt.id;
  if (filterActive && ((id & filterMask) != filterId)) pkt.hide = true;
  else if (hideBatt && (id == 0x10C)) pkt.hide = true;
  // _(pkt);

  if (rowCount == 1000) {
    $tbodyRaw.find('.tr').first().remove();
    $tbodyDecoded.find('.tr').first().remove();
  } else {
    rowCount++;
    $('#rowCount').text(rowCount);
  }

  let rendered = Mustache.render(templateRaw, pkt);
  $tbodyRaw.append(rendered);
  if (autoScroll && !pkt.hide) $tbodyRaw[0].scroll({ top: $tbodyRaw[0].scrollHeight });

  rendered = Mustache.render(templateDecoded, pkt);
  $tbodyDecoded.append(rendered);
  if (autoScroll && !pkt.hide) $tbodyDecoded[0].scroll({ top: $tbodyDecoded[0].scrollHeight });
}

$('#macroPanel').on('click', '.btnMacro', function (e) {
  const $this = $(this);
  const macro = $this.data('json');
  const data = macro.data || [];
  _(macro, data);
  ws.send(JSON.stringify({
    id: 0,
    type: 'CANtx',
    pkt: {
      id: macro.id,
      rr: macro.rr ? 1 : 0,
      data: data
    }
  }));
});

$('#btnToggleMacros').on('click', function(e) {
  $('#macroPanel').toggle();
});

function _(...v) {
  console.log(...v);
}
