"use strict";

class Decoder {
  process(pkt) {
    pkt.seconds = (pkt.millis / 1000).toFixed(2);
    pkt.idhex = this.int2hex(pkt.id, 3);
    pkt.dataOut = [];
    for (var i = 0; i < pkt.data.length; i++) pkt.dataOut[i] = `0x${this.int2hex(pkt.data[i], 2)}`;
    pkt.dataOut = pkt.dataOut.join(' ');
    pkt.decoded = pkt.data.join(' ');
    if ((pkt.id & 0x700) == 0x700) {
      pkt.type = CAN_IDS['700'].name;
      if (typeof this[pkt.type] === 'function' && !pkt.rr) pkt.decoded = this[pkt.type](pkt);
    } else if (CAN_IDS[pkt.idhex] && pkt.id != 0x300) {
      pkt.type = CAN_IDS[pkt.idhex].name;
      if (typeof this[pkt.type] === 'function' && !pkt.rr) pkt.decoded = this[pkt.type](pkt);
    } else if ((pkt.id & 0x700) == 0x300) {
      pkt.type = CAN_IDS['300'].name;
      if (typeof this[pkt.type] === 'function' && !pkt.rr) pkt.decoded = this[pkt.type](pkt);
    }
  }

  int2hex(i, p) {
    let s = '000000000' + i.toString(16);
    return s.substring(s.length - p).toUpperCase();
  }

  STATUS_RC_VT(pkt) {
    _('STATUS_RC_VT', pkt);
    if (pkt.dl < 8) return `ERROR: expected 8 bytes, rec'd ${pkt.dl}`;
    const mainV = ((pkt.data[0] << 8 | pkt.data[1]) / 10).toFixed(1);
    const logicV = ((pkt.data[2] << 8 | pkt.data[3]) / 10).toFixed(1);
    const temp1 = ((pkt.data[4] << 8 | pkt.data[5]) / 10).toFixed(1);
    const temp2 = ((pkt.data[6] << 8 | pkt.data[7]) / 10).toFixed(1);
    return `Main V: ${mainV} Temp 1: ${temp1}°C`;
  }

  STATUS_BATT(pkt) {
    _('STATUS_BATT', pkt);
    if (pkt.dl < 8) return `ERROR: expected 8 bytes, rec'd ${pkt.dl}`;
    const avgV = ((pkt.data[0] << 8 | pkt.data[1]) / 1000).toFixed(1);
    const maxV = ((pkt.data[2] << 8 | pkt.data[3]) / 1000).toFixed(1);
    const avgI = (((pkt.data[4] << 8 | pkt.data[5]) - 32768) / 1000).toFixed(3);
    const maxI = (((pkt.data[6] << 8 | pkt.data[7]) - 32768) / 1000).toFixed(3);
    return `Avg V: ${avgV} Avg I: ${avgI}`;
  }

  STATUS_ARM(pkt) {
    _('STATUS_ARM', pkt);
    if (pkt.dl < 3) return `ERROR: expected  bytes, rec'd ${pkt.dl}`;
    return `State: ${pkt.data[0]} Moving: ${pkt.data[1]} Error: ${pkt.data[2]}`;
  }

  STATUS_CAN_POS(pkt) {
    _('STATUS_CAN_POS', pkt);
    if (pkt.dl < 6) return `ERROR: expected 6 bytes, rec'd ${pkt.dl}`;
    return `Can: ${bytes2int16(pkt.data[0], pkt.data[1])}, ${bytes2int16(pkt.data[2], pkt.data[3])}, ${bytes2int16(pkt.data[4], pkt.data[5])}`;
  }

  STATUS_GRIP_POS(pkt) {
    _('STATUS_GRIP_POS', pkt);
    if (pkt.dl < 6) return `ERROR: expected 6 bytes, rec'd ${pkt.dl}`;
    return `Grip: ${bytes2int16(pkt.data[0], pkt.data[1])}, ${bytes2int16(pkt.data[2], pkt.data[3])}, ${bytes2int16(pkt.data[4], pkt.data[5])}`;
  }

  STATUS_RC_STAT(pkt) {
    _('STATUS_RC_STAT', pkt);
    if (pkt.dl < 1) return `ERROR: expected 1 byte, rec'd ${pkt.dl}`;
    var s;
    if (pkt.data[0] == 0) {
      s = 'Not moving';
    } else {
      s = `Moving`;
    }
    return s;
  }

  STATUS_IMU(pkt) {
    _('STATUS_IMU', pkt);
    if (pkt.dl != 6) return `ERROR: expected 6 bytes, rec'd ${pkt.dl}`;
    const x = (bytes2int16(pkt.data[0], pkt.data[1]) / 10).toFixed(1);
    const y = (bytes2int16(pkt.data[2], pkt.data[3]) / 10).toFixed(1);
    const z = (bytes2int16(pkt.data[4], pkt.data[5]) / 10).toFixed(1);
    return `X: ${x} Y: ${y} Z: ${z}`;
  }

  STATUS_WALL_POS(pkt) {
    _('STATUS_WALL_POS', pkt);
    if (pkt.dl != 8) return `ERROR: expected 8 bytes, rec'd ${pkt.dl}`;
    const lz = (bytes2int16(pkt.data[2], pkt.data[3]));
    const rx = (bytes2int16(pkt.data[4], pkt.data[5]));
    const rz = (bytes2int16(pkt.data[6], pkt.data[7]));
    return `Straight: ${lz} Corner: ${rx}, ${rz}`;
  }

  STATUS_FRIG_POS(pkt) {
    _('STATUS_FRIG_POS', pkt);
    if (pkt.dl != 8) return `ERROR: expected 8 bytes, rec'd ${pkt.dl}`;
    const lx = (bytes2int16(pkt.data[0], pkt.data[1]));
    const lz = (bytes2int16(pkt.data[2], pkt.data[3]));
    const rx = (bytes2int16(pkt.data[4], pkt.data[5]));
    const rz = (bytes2int16(pkt.data[6], pkt.data[7]));
    return `Left: ${lx}, ${lz} Right: ${rx}, ${rz}`;
  }

  STATUS_DOOR_POS(pkt) {
    _('STATUS_DOOR_POS', pkt);
    if (pkt.dl < 4) return `ERROR: expected 4 bytes, rec'd ${pkt.dl}`;
    const lx = (bytes2int16(pkt.data[0], pkt.data[1]));
    const lz = (bytes2int16(pkt.data[2], pkt.data[3]));
    return `Left: ${lx}, ${lz}`;
  }

  STATUS_INSIDE_POS(pkt) {
    _('STATUS_INSIDE_POS', pkt);
    if (pkt.dl != 8) return `ERROR: expected 8 bytes, rec'd ${pkt.dl}`;
    const lx = (bytes2int16(pkt.data[0], pkt.data[1]));
    const lz = (bytes2int16(pkt.data[2], pkt.data[3]));
    const rx = (bytes2int16(pkt.data[4], pkt.data[5]));
    const rz = (bytes2int16(pkt.data[6], pkt.data[7]));
    return `Frig: ${lx}, ${lz} Can: ${rx}, ${rz}`;
  }

  STATUS_CAN_POS(pkt) {
    _('STATUS_CAN_POS', pkt);
    if (pkt.dl != 6) return `ERROR: expected 6 bytes, rec'd ${pkt.dl}`;
    const x = (bytes2int16(pkt.data[0], pkt.data[1]) / 10).toFixed(1);
    const y = (bytes2int16(pkt.data[2], pkt.data[3]) / 10).toFixed(1);
    const z = (bytes2int16(pkt.data[4], pkt.data[5]) / 10).toFixed(1);
    return `X: ${x} Y: ${y} Z: ${z}`;
  }

  STATUS_GRIP_POS(pkt) {
    _('STATUS_GRIP_POS', pkt);
    if (pkt.dl != 6) return `ERROR: expected 6 bytes, rec'd ${pkt.dl}`;
    const x = (bytes2int16(pkt.data[0], pkt.data[1]) / 10).toFixed(1);
    const y = (bytes2int16(pkt.data[2], pkt.data[3]) / 10).toFixed(1);
    const z = (bytes2int16(pkt.data[4], pkt.data[5]) / 10).toFixed(1);
    return `X: ${x} Y: ${y} Z: ${z}`;
  }

  STATUS_TABLE_POS(pkt) {
    _('STATUS_TABLE_POS', pkt);
    if (pkt.dl != 6) return `ERROR: expected 6 bytes, rec'd ${pkt.dl}`;
    const x = (bytes2int16(pkt.data[0], pkt.data[1]) / 10).toFixed(1);
    const y = (bytes2int16(pkt.data[2], pkt.data[3]) / 10).toFixed(1);
    const z = (bytes2int16(pkt.data[4], pkt.data[5]) / 10).toFixed(1);
    return `X: ${x} Y: ${y} Z: ${z}`;
  }

  RC_DIRECT(pkt) {
    var cmd = (pkt.id & 0xFF);
    // _('cmd', cmd);
    pkt.type = `RC Direct: ${cmd}`;
    return pkt.decoded;
  }

  CMD_SPEAK(pkt) {
    _('CMD_SPEAK', pkt);
    if (pkt.dl < 1) return `ERROR: expected 1-2 bytes, rec'd ${pkt.dl}`;
    let group = 'unknown';
    if (pkt.dl == 1) {
      if (pkt.data[0] in TS_GROUPS) group = TS_GROUPS[pkt.data[0]];
      return `Group: ${group}`;
    } else {
      let vol = pkt.data[0];
      if (pkt.data[1] in TS_GROUPS) group = TS_GROUPS[pkt.data[1]];
      return `Group: ${group} Vol: ${vol}`;
    }
  }

  SPEAK_DIRECT(pkt) {
    _('SPEAK_DIRECT', pkt);
    if (pkt.dl == 0) return `ERROR: expected 1-8 bytes, rec'd ${pkt.dl}`;
    let s = 'Say: ';
    for (var i = 0; i < pkt.dl; i++) {
      if (pkt.data[i] == 13) s = s + ' [say it]';
      else s = s + String.fromCharCode(pkt.data[i]);
    }
    return s;
  }

  CMD_VISION(pkt) {
    _('CMD_VISION', pkt);
    if (pkt.dl < 2) return `ERROR: expected 2 bytes, rec'd ${pkt.dl}`;
    if (pkt.data[0] == 0x01) {
      return `D435 angle: ${pkt.data[1]}`;
    } else {
      return pkt.decoded;
    }
  }

  CMD_IMU(pkt) {
    _('CMD_IMU', pkt);
    var s;
    if (pkt.dl < 1) return `ERROR: expected 1 bytes, rec'd ${pkt.dl}`;
    if (pkt.data[0] == 0) {
      s = 'No updates';
    } else {
      s = `Updates every ${pkt.data[0]}0 ms`;
    }
    if (pkt.dl > 1 && pkt.data[1]) s = s + ', reset to 0';
    return s;
  }

  CMD_HEADLIGHT(pkt) {
    _('CMD_HEADLIGHT', pkt);
    if (pkt.dl < 1) return `ERROR: expected 1 byte, rec'd ${pkt.dl}`;
    return `Headlight PWM: ${pkt.data[0]}`;
  }

  CMD_SUPER(pkt) {
    _('CMD_SUPER', pkt);
    if (pkt.dl < 1) return `ERROR: expected at least 1 byte, rec'd ${pkt.dl}`;
    if (pkt.data[0] == 0x00) {
      return `State: ${pkt.data[1]}`;
    } else if (pkt.data[0] == 0x01) {
      return `Speed: ${(pkt.data[1] / 10).toFixed(1)}`;
    } else {
      return pkt.decoded;
    }
  }

  CMD_RC_MOVE(pkt) {
    _('CMD_RC_MOVE', pkt);
    if (pkt.dl < 3) return `ERROR: expected at least 3 byte, rec'd ${pkt.dl}`;
    return this.move(pkt);
  }

  CMD_RC_MOVE_DIST(pkt) {
    _('CAN_CMD_RC_MOVE_DIST', pkt);
    if (pkt.dl < 5) return `ERROR: expected at least 5 byte, rec'd ${pkt.dl}`;
    return this.move(pkt);
  }

  CMD_RC_MOVE_ANGLE(pkt) {
    _('CAN_CMD_RC_MOVE_ANGLE', pkt);
    if (pkt.dl < 5) return `ERROR: expected at least 5 byte, rec'd ${pkt.dl}`;
    return this.move(pkt);
  }

  move(pkt) {
    const immediate = pkt.data[0] >> 7 ? 'immed' : 'buf\'d'; // immediate = 1, buffered = 0
    const end_stop = (pkt.data[0] >> 6) & 0x01 ? 'stop' : 'coast'; // stop = 1, coast = 0
    const fr = (pkt.data[0] >> 5) & 0x01 ? 'forward' : 'reverse'; // forward = 1, reverse = 0
    const speed = pkt.data[0] & 0x1F; // 0 .. 17
    var lr = pkt.data[1] >> 7 ? 'left' : 'right'; // left = 1, right = 0
    var radius = (pkt.data[1] & 0x7F) << 8 | pkt.data[2]; // 0 .. 32767 (RADIUS_STRAIGHT)
    if (radius == 32767) {
      lr = 'straight';
      radius = '';
    } else {
      radius = `radius:${radius}`;
    }
    var s = `${immediate} ${end_stop} ${fr} sp:${speed} ${lr} ${radius}`;
    const distAngle = 0;
    if (pkt.id == 0x209 || pkt.id == 0x20A) {
      const distAngle = pkt.data[3] << 8 | pkt.data[4];
      if (pkt.id == 0x209)
        s = s + ` dist:${distAngle}`;
      else
        s = s + ` ang:${distAngle}`;
    }
    return s;
  }

  CMD_RC_MOVE_TO(pkt) {
    _('CMD_RC_MOVE_TO', pkt);
    if (pkt.dl < 5) return `ERROR: expected at least 5 byte, rec'd ${pkt.dl}`;

    const x = bytes2int16(pkt.data[0], pkt.data[1]);
    const z = bytes2uint16(pkt.data[2], pkt.data[3]);
    const immediate = pkt.data[4] >> 7 ? 'immed' : 'buf\'d'; // immediate = 1, buffered = 0
    const end_stop = (pkt.data[4] >> 6) & 0x01 ? 'stop' : 'coast'; // stop = 1, coast = 0
    const speed = pkt.data[4] & 0x1F; // 0 .. 17
    const heading = bytes2int16(pkt.data[5], pkt.data[6]); // 0 .. 359, 999 = none
    var s = `x:${x} z:${z} ${immediate} ${end_stop} sp:${speed} head:${heading}°`;
    return s;
  }

  CMD_RC_ALIGN_HEADING(pkt) {
    _('CMD_RC_ALIGN_HEADING', pkt);
    if (pkt.dl < 3) return `ERROR: expected at least 3 byte, rec'd ${pkt.dl}`;

    const heading = bytes2int16(pkt.data[0], pkt.data[1]); // 0 .. 359
    var s = `head:${heading}° sp:${pkt.data[2]} QPPS`;
    return s;
  }

  ARM_JOG(pkt) {
    _('ARM_JOG', pkt);
    if (pkt.dl < 4) return `ERROR: expected at least 4 byte, rec'd ${pkt.dl}`;

    const joint = pkt.data[0];
    const ang = bytes2int16(pkt.data[1], pkt.data[2]) / 10;
    const velocityFactor = pkt.data[3];
    var s = `joint:${joint} delta:${ang}° vf:${velocityFactor}`;
    return s;
  }

  ARM_MOVETO_A(pkt) {
    _('ARM_MOVETO_A', pkt);
    if (pkt.dl < 7) return `ERROR: expected at least 7 byte, rec'd ${pkt.dl}`;

    const x = bytes2int16(pkt.data[0], pkt.data[1]);
    const y = bytes2int16(pkt.data[2], pkt.data[3]);
    const z = bytes2int16(pkt.data[4], pkt.data[5]);
    const go = pkt.data[6];
    var s = `x:${x} y:${y} z:${z} go:${go}`;
    return s;
  }

  ARM_MOVETO_B(pkt) {
    _('ARM_MOVETO_B', pkt);
    if (pkt.dl < 5) return `ERROR: expected at least 5 byte, rec'd ${pkt.dl}`;

    const r = bytes2int16(pkt.data[0], pkt.data[1]);
    const p = pkt.data[2];
    const velocityFactor = pkt.data[3];
    const go = pkt.data[4];
    var s = `r:${r} p:${p} vf:${velocityFactor} go:${go}`;
    return s;
  }

  LOGGING(pkt) {
    const SOURCES = [
      'Logger', // 0
      'gdgt1 TTS', // 1
      'gdgt2 Vision', // 2
      'gdgt3 Supervisor', // 3
      '?', //4
      '?', //5
      'CANduino sensor left', // 6
      'CANduino sensor right', // 7
      'CANduino sensor top', // 8
      'CANduino motor', // 9
      'CANduino LCD', // 10
      'Arm controller' // 11
    ];

    const TYPES = [
      'bool', // 0
      'char', // 1
      'uint8', // 2
      'int8', // 3
      'uint16', // 4
      'int16', // 5
      'uint32', // 6
      'int32', // 7
      'float', // 8
      'double', // 9
      '?',
      '?',
      '?',
      '?',
      '?',
      'str' // 15
    ];

    var isource = (pkt.id & 0xF0) >> 4;
    // _('isource', isource);
    var source = isource < SOURCES.length ? SOURCES[isource] : `Unknown (${isource})`;
    // _('source', source);

    var itype = pkt.id & 0x0F;
    // _('itype', itype);
    var type = itype < TYPES.length ? TYPES[itype] : '?';
    // _('type', type);

    if (type == 'str') {
      var str = '';
      if (pkt.dl > 0) {
        for (var i = 0; i < pkt.dl; i++) {
          str = str + String.fromCharCode(pkt.data[i]);
        }
      }
      pkt.type = source;
      return `***** ${str} *****`;
    } else if (type == '?') {
      return pkt.decoded;
    } else {
      var v;
      var dl;

      if (type == 'bool') {
        v = pkt.data[0] ? 'true' : 'false';
        dl = 1;
      } else if (type == 'char') {
        v = `${pkt.data[0]} (${String.fromCharCode(pkt.data[0])})`;
        dl = 1;
      } else if (type == 'uint8') {
        v = pkt.data[0];
        dl = 1;
      } else if (type == 'int8') {
        v = pkt.data[0];
        if (v & 0x80) { // sign bit test
          v = 0xFFFFFF00 | v;  // fill in most significant bits with 1's
        }
        dl = 1;
      } else if (type == 'uint16') {
        v = bytes2uint16(pkt.data[0], pkt.data[1]);
        dl = 2;
      } else if (type == 'int16') {
        v = bytes2int16(pkt.data[0], pkt.data[1]);
        dl = 2;
      } else if (type == 'uint32') {
        v = bytes2uint32(pkt.data[0], pkt.data[1], pkt.data[2], pkt.data[3]);
        dl = 4;
      } else if (type == 'int32') {
        v = bytes2int32(pkt.data[0], pkt.data[1], pkt.data[2], pkt.data[3]);
        dl = 4;
      } else if (type == 'float') {
        v = bytes2float(pkt.data[3], pkt.data[2], pkt.data[1], pkt.data[0]).toFixed(3);
        dl = 4;
      } else {
        dl = 0;
      }
      var str = '';
      if (dl > 0) {
        for (var i = dl; i < pkt.dl; i++) {
          str = str + String.fromCharCode(pkt.data[i]);
        }
      }
      str = `${str} `;

      pkt.type = source;
      return `${str}(${type}): ${v}`;
    }
  }
}

function bytes2int16(bh, bl) {
  var x = (((bh & 0xFF) << 8) | (bl & 0xFF));
  if (bh & 0x80) { // sign bit test
    x = 0xFFFF0000 | x;  // fill in most significant bits with 1's
  }
  return x;
}

function bytes2uint16(bh, bl) {
  return (((bh & 0xFF) << 8) | (bl & 0xFF));
}

function bytes2int32(b3, b2, b1, b0) {
  var x = (((b3 & 0xFF) << 24) | ((b2 & 0xFF) << 16) | ((b1 & 0xFF) << 8) | (b0 & 0xFF));
  // if (bh & 0x80) { // sign bit test
  //   x = 0xFFFF0000 | x;  // fill in most significant bits with 1's
  // }
  return x;
}

function bytes2uint32(b3, b2, b1, b0) {
  return (((b3 & 0xFF) << 24) | ((b2 & 0xFF) << 16) | ((b1 & 0xFF) << 8) | (b0 & 0xFF)) >>> 0;
}

function bytes2float(b3, b2, b1, b0) {
  const buffer = new ArrayBuffer(4);
  const view = new DataView(buffer);
  view.setUint8(0, b3);
  view.setUint8(1, b2);
  view.setUint8(2, b1);
  view.setUint8(3, b0);
  return view.getFloat32(0);
}
