const CAN_IDS = {
	'000': {name: 'ALERT_BUMPER Left'},
	'001': {name: 'ALERT_BUMPER Right'},
	'002': {name: 'ALERT_DROP Left'},
	'003': {name: 'ALERT_DROP Right'},
	'004': {name: 'ALERT_EMERG_STOP'},
	'006': {name: 'ALERT_KILL'},
	'008': {name: 'ALERT_RC_STATUS'},
	'00A': {name: 'ALERT_RC_CUR_ALARM'},

	'100': {name: 'STATUS_BUMPER Left'},
	'101': {name: 'STATUS_BUMPER Right'},
	'102': {name: 'STATUS_DROP Left'},
	'103': {name: 'STATUS_DROP Right'},
	'104': {name: 'STATUS_SONIC Left'},
	'105': {name: 'STATUS_SONIC Right'},
	'106': {name: 'STATUS_KILL'},
	'108': {name: 'STATUS_RC_STAT'},
	'109': {name: 'STATUS_RC_ENC'},
	'10A': {name: 'STATUS_RC_VT'},
	'10B': {name: 'STATUS_RC'},
	'10C': {name: 'STATUS_BATT'},
	'10D': {name: 'STATUS_DRIVING'},
	'10E': {name: 'STATUS_VISION'},
	'10F': {name: 'STATUS_IMU'},
	'110': {name: 'STATUS_HEADLIGHT'},
	'17F': {name: 'STATUS_SPEAK'},
	'180': {name: 'STATUS_RTC'},
	'1A0': {name: 'STATUS_ARM'},

	'1D2': {name: 'STATUS_WALL_POS'},
	'1D3': {name: 'STATUS_FRIG_POS'},
	'1D4': {name: 'STATUS_DOOR_POS'},
	'1D5': {name: 'STATUS_INSIDE_POS'},
	'1D6': {name: 'STATUS_CAN_POS'},
	'1D7': {name: 'STATUS_GRIP_POS'},
	'1D8': {name: 'STATUS_TABLE_POS'},
	'1D9': {name: 'STATUS_HOME_POS'},
	'1DA': {name: 'STATUS_VIS_OBST'},

	'1F0': {name: 'STATUS_SUPER'},

	'204': {name: 'CMD_SONIC'},
	'208': {name: 'CMD_RC_MOVE'},
	'209': {name: 'CMD_RC_MOVE_DIST'},
	'20A': {name: 'CMD_RC_MOVE_ANGLE'},
	'20B': {name: 'CMD_RC_MOVE_JOG'},
	'20C': {name: 'CMD_RC_MOVE_TO'},
	'20D': {name: 'CMD_RC_PARAMS'},
	'20E': {name: 'CMD_RC_ALIGN_HEADING'},
	'20F': {name: 'CMD_RC_MOTORS_OFF'},
	'220': {name: 'CMD_VISION'},
	'230': {name: 'CMD_IMU'},
	'231': {name: 'CMD_HEADLIGHT'},
	'27F': {name: 'CMD_SPEAK'},
	'280': {name: 'CMD_RTC'},
	'2F0': {name: 'CMD_SUPER'},
	'2FE': {name: 'CMD_QUIT'},
	'2FF': {name: 'CMD_HALT'},

	'300': {name: 'RC_DIRECT'},
	'3F0': {name: 'RC_JOYSTICK'},
	'3F1': {name: 'RC_MOVE_1JOYSTICK'},
	'3F2': {name: 'RC_MOVE_2JOYSTICK'},

	'400': {name: 'ARM_STOP'},
	'401': {name: 'ARM_STOP_DECEL'},
	'402': {name: 'ARM_LIMP'},
	'403': {name: 'ARM_INIT'},
	'404': {name: 'ARM_PARK'},
	'405': {name: 'ARM_UNPARK'},
	'406': {name: 'ARM_EXTEND'},
	'407': {name: 'ARM_DOOR_EXT'},
	'408': {name: 'ARM_HOME'},
	'409': {name: 'ARM_HOME_R'},
	'40A': {name: 'ARM_HOME_P'},
	'40B': {name: 'ARM_TOOL_READY'},
	'40C': {name: 'ARM_TOOL_GET'},
	'40D': {name: 'ARM_TOOL_LOCK'},
	'40E': {name: 'ARM_TOOL_RETURN'},
	'40F': {name: 'ARM_SEQUENCE'},
	'410': {name: 'ARM_DOOR_PRESS'},

	'420': {name: 'ARM_DISABLE'},
	'421': {name: 'ARM_ENABLE'},
	'422': {name: 'ARM_STATUS'},
	'423': {name: 'ARM_SET_VF'},
	'432': {name: 'ARM_MOVETO_A'},
	'433': {name: 'ARM_MOVETO_B'},
	'434': {name: 'ARM_SEEK'},
	'435': {name: 'ARM_JOG'},
	'440': {name: 'ARM_SETPOS'},
	'450': {name: 'ARM_LED'},
	'451': {name: 'ARM_NEO'},

	'460': {name: 'ARM_GRAB_CAN'},

	'480': {name: 'GRIP_OPEN'},
	'481': {name: 'GRIP_CLOSE'},
	'482': {name: 'GRIP_REF_LED'},
	'483': {name: 'GRIP_FINGERS'},

	'600': {name: 'SPEAK_DIRECT'},

	'700': {name: 'LOGGING'},


};
