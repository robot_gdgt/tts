#!/usr/bin/python3

"""
Capture image data from RealSense camera and store in POSIX IPC shared memory
"""

import sys
import subprocess
import time
import can
from can_ids import *
from var_dump import var_dump as vd
from gpiozero import LED

if __name__ == "__main__":

    ##### SETUP #####
    print("Start setup")
    hbLED = LED(21)
    hbLED.on()

    print("ip link set can0 down")
    subprocess.run("sudo ip link set can0 down", shell=True)
    print("ip link set can0 up")
    subprocess.run("sudo ip link set can0 up type can bitrate {}".format(CAN_BUS_SPEED), shell=True)
    print("can.interface.Bus")
    bus = can.interface.Bus(bustype='socketcan_native', channel='can0')
    can_filters = [
#         {"can_id": CAN_CMD_HALT, "can_mask": 0x7FF, "extended": False, "req_by": {"sys",}},
        {"can_id": CAN_CMD_HALT, "can_mask": 0x7FF, "extended": False},
    ]
    print("bus.set_filters")
    bus.set_filters(can_filters)

    print("setup done")
    hbLED.off()
    hbTime = time.time() + 1.950

    ##### LOOP #####
    try:

        while True:
            now = time.time()

            # Get CAN messages and add them to the Redis queue
            new_msg = bus.recv(0.0)
            if new_msg is not None:
                print(new_msg)
                msg = {
                    'can_id': new_msg.arbitration_id,
                    'is_remote_frame': new_msg.is_remote_frame,
                    'dlc': new_msg.dlc,
                    'data': new_msg.data
                }
                print('CANrx', msg)

            # Blink the LED
            if now > hbTime:
                if hbLED.is_lit:
                    hbLED.off()
                    hbTime = now + 1.950
                else:
                    hbLED.on()
                    hbTime = now + 0.050

    except KeyboardInterrupt:
        pass
