#!/usr/bin/python3

# import sys
# import subprocess
# import random
# import math
from pygame import mixer
# from time import sleep, time
# from datetime import datetime
# from os import path
# import json
# import logging
# import redis
# import sqlite3
# from sqlite3 import Error
from status_led import StatusLED
from can_ids import *
from tts_groups import *
# from var_dump import var_dump as vd

# FILE = path.splitext(path.basename(__file__))[0]

# logging.basicConfig(
# #     filename='test.log',
#     level=logging.INFO,
#     format='%(asctime)s %(levelname)s: %(message)s',
#     datefmt='%H:%M:%S'
# )
# logging.info('START')

def playWav(file, vol):
    print(f"Play {file}")
#     sound = mixer.Sound(file)
    mixer.music.load(file)
#     sound.set_volume(vol)
    mixer.music.set_volume(vol)
    print(f"Play with vol {vol}")
#     return sound.play() # returns channel
    return mixer.music.play() # returns channel

if __name__ == '__main__':

    ##### SETUP #####
    hbLED = StatusLED(25, True)

    mixer.init(16000)
    playWav('/home/pi/phrases/1016.wav', 0.5)

    hbLED.heartbeat()

    ##### LOOP #####
    try:

        while True:

            # Blink the LED
            hbLED.tickle()

    except KeyboardInterrupt:
        mixer.quit()
