"""
Text-to-speech
"""

from datetime import datetime
import json
import logger
import logging
import math
from os import path
from pygame import mixer
import random
import sqlite3
from sqlite3 import Error
import subprocess
import sys
from time import sleep, time

from status_led import StatusLED
import can_bus_thread
from gdgt_platform import *
from can_ids import *
from tts_groups import *
from RollingMedian import RollingMedian
import wii_remote_thread as wii
# from var_dump import var_dump as vd

hbLED = StatusLED(LED_PIN, True)

FILE = path.splitext(path.basename(__file__))[0]

logger.init(None, logging.INFO)

# App state object class
class AppState:
    def __init__(self):
        self.state = CAN_CMD_PGM_RUNNING
        self.defaultVol = 0.2
        self.drive = 0 # 0 = not driving, 1 = waiting to center, 2 = driving

state = AppState()

##### TTS Functions #####

REPEAT_DELAY = 4

lastPlayed = {}

'''
playSoundFromGroup(con, idgroup, vol)
    getFromGroup(con, idgroup) -> row
    playWav(file, vol) -> channel
    or
    sayString(id, str, vol) -> playWav
        playWav(file, vol) -> channel
'''

def sqlConnection():
    try:
        con = sqlite3.connect('/home/pi/gdgt_tts/src/strings.db')
        con.row_factory = sqlite3.Row
        return con
    except sqlite3.Error as e:
        logging.error(f'sqlite3.connect error: {e}')
        raise Exception('Failed to open sqlite db')

def getFromGroup(con, idgroup):
    logging.info(f"Say from group {idgroup}")
    cur = con.cursor()
    cur.execute("select count(*) from strings where idgroup = ?", (idgroup,))
    row = cur.fetchone()
    if row[0] == 0:
        logging.debug("Not found")
        return None
    n = random.randrange(0, math.trunc(row[0] / 2 + 0.5))
    cur.execute("select id, text, last_used from strings where idgroup = ? order by last_used limit 1 offset ?", (idgroup, n))
    row = cur.fetchone()
    if row:
#       logging.debug(row['id'])
        now = datetime.now()
        cur.execute("update strings set last_used = ? where id = ?", (now.timestamp(), row['id']))
        con.commit()
        return row
    else:
        return None

def getFromID(con, id):
    logging.info(f"Say ID {id}")
    cur = con.cursor()
    cur.execute("select id, text from strings where id = ?", (id,))
    row = cur.fetchone()
    return row

def sayString(id, str, vol):
#   logging.debug(str)
    cmd = f"pico2wave -w /home/pi/phrases/{id}.wav \"{str}\""
    logging.debug(cmd)
    subprocess.run([cmd], shell=True)
    return playWav(f"/home/pi/phrases/{id}.wav", vol)

def playWav(file, vol):
    if mixer.music.get_busy():
        logging.debug("Is busy")
        mixer.music.fadeout(500)
    logging.info(f"Play {file}")
    mixer.music.load(file)
    mixer.music.set_volume(vol)
    logging.debug(f"Play with vol {vol}")
    return mixer.music.play() # returns channel

def playSoundFromGroup(con, idgroup, vol):
    now = datetime.now().timestamp()
    if idgroup in lastPlayed and now < lastPlayed[idgroup] + REPEAT_DELAY:
        return None

    snd = getFromGroup(con, idgroup)
    if snd is not None:
        lastPlayed[idgroup] = now
        if snd['text'].endswith('.wav'):
            logging.debug(f"/home/pi/gdgt_tts/sounds/{snd['text']}")
            return playWav(f"/home/pi/gdgt_tts/sounds/{snd['text']}", vol)
        else:
            if path.exists(f"/home/pi/phrases/{snd['id']}.wav"):
                return playWav(f"/home/pi/phrases/{snd['id']}.wav", vol)
            else:
                return sayString(snd['id'], snd['text'], vol)

def playSoundFromID(con, id, vol):
    snd = getFromID(con, id)
    if snd is not None:
        if snd['text'].endswith('.wav'):
            logging.debug(f"/home/pi/gdgt_tts/sounds/{snd['text']}")
            return playWav(f"/home/pi/gdgt_tts/sounds/{snd['text']}", vol)
        else:
            if path.exists(f"/home/pi/phrases/{snd['id']}.wav"):
                return playWav(f"/home/pi/phrases/{snd['id']}.wav", vol)
            else:
                return sayString(snd['id'], snd['text'], vol)

##### CAN MSG RECD #####

def canBusMsgRecd(msg):
    global prevX, prevY
#     print(msg)
    if msg['can_id'] == CAN_CMD_SPEAK:
        if msg['dlc'] == 1:
            playSoundFromGroup(con, msg['data'][0], state.defaultVol)
        elif msg['dlc'] > 1:
            if msg['data'][0] == 0: # set volume
                state.defaultVol = msg['data'][1] / 512
                # TODO add saving state.defaultVol to config file and read it back in later
                data = {'defaultVol': state.defaultVol}
                with open('tts.ini', 'w') as outfile:
                    json.dump(data, outfile)
            else:
                if msg['data'][0] == 1:
                    vol = state.defaultVol
                else:
                    vol = msg['data'][0] / 512
                d = 0
                for i in range(1, msg['dlc']):
                    d = d * 256 + msg['data'][i]
                if d < 1000:
                    playSoundFromGroup(con, d, vol)
                else:
                    playSoundFromID(con, d, vol)

    elif msg['can_id'] & 0x700 == CAN_SPEAK_DIRECT:
        adhocId = msg['can_id'] & 0x0FF
        if adhocId not in adhocStrings:
            adhocStrings[adhocId] = ''
        isFinal = False
        for c in msg['data']:
            if c == 10 or c == 0: # new line or null terminator
                isFinal = True
                break
            adhocStrings[adhocId] += chr(c)
        if isFinal:
            if len(adhocStrings[adhocId]) > 0:
                sayString('adhoc', adhocStrings[adhocId], state.defaultVol)
                adhocStrings[adhocId] = ''

    elif msg['can_id'] == CAN_CMD_JOYSTICK:
        if msg['data'][0]:
            state.drive = 1
            rmX.reset()
            rmY.reset()
            prevX = 0
            prevY = 0
        else:
            state.drive = 0

    elif msg['can_id'] == CAN_CMD_HALT:
        channel = playSoundFromGroup(con, SPEAK_SHUTDOWN, state.defaultVol)
        logging.info('shutdown')
        state.state = CAN_CMD_PGM_QUIT

##### SETUP #####

if path.exists('tts.ini'):
    with open('tts.ini') as infile:
        data = json.load(infile)
else:
    data = None

if data:
    state.defaultVol = data['defaultVol']

can_bus_thread.msgRecdCallback = canBusMsgRecd
can_filters = [
    {"can_id": CAN_CMD_SPEAK, "can_mask": 0x7FF, "extended": False},
    {"can_id": CAN_SPEAK_DIRECT, "can_mask": 0x700, "extended": False},
    {"can_id": CAN_CMD_JOYSTICK, "can_mask": 0x7FF, "extended": False},
    {"can_id": CAN_CMD_HALT, "can_mask": 0x7FF, "extended": False},
]
can_bus_thread.set_filters(can_filters)

random.seed()
mixer.init(16000)

con = sqlConnection()

playSoundFromGroup(con, SPEAK_STARTUP, state.defaultVol)

adhocStrings = {}

rmX = RollingMedian(9)
rmY = RollingMedian(9)
prevX = None
prevY = None

hbLED.heartbeat()

##### LOOP #####

logging.warning('READY');

try:

    while True:

#         now = time()

        # Monitor Wii remote
        if state.drive:
            x,y,z = wii.accel()

            rmX.add(x)
            x = rmX.get()
            rmY.add(y)
            y = rmY.get()
            if x != prevX or y != prevY:
                if state.drive == 2:
                    can_bus_thread.send({
                        'can_id': CAN_CMD_RC_MOVE,
                        'data': [x, y],
                    })
                prevX = x
                prevY = y
            elif state.drive == 1:
                if x == 128 and prevX == 128 and y == 128 and prevY == 128:
                    state.drive = 2

        # Blink the LED
        hbLED.tickle()

        if state.state == CAN_CMD_PGM_QUIT or state.state == CAN_CMD_HALT:
            break

        sleep(0.001)

except KeyboardInterrupt:
    logging.info('KeyboardInterrupt')

can_bus_thread.down()

if state.state == CAN_CMD_HALT:
    sleep(5)
    subprocess.run("sudo halt", shell=True)

"""
CAN data:
DLC == 1 means play sound from group 1 - 255 at default volume
data[0] == 0 set default volume = data[1]
data[0] == 1 play sound = data[1...n]
data[0] > 1 play sound = data[1...n] at vol = data[0]

sound = 1...999 = play randomly from group
sound > 1000 = play specific sound
"""
